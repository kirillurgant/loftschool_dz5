const express = require('express');
const app = express();
const { resolve } = require('path');

app.use(express.static(resolve(__dirname, 'dist')));

app.get('*', function (req, res) {
  res.sendFile(resolve(__dirname, 'dist/index.html'));
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
